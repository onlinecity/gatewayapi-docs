Legacy SOAP API
===============

This is very much a work in progress, for now refer to our old documentation.

You can find it on https://oc.dk/gateway/#api or embedded below if reading
online.

.. raw:: html

   <iframe align="top" frameborder="0" height="940px" scrolling="yes" src="https://docs.google.com/document/pub?id=12SIngUxSgtgRt_Sc-zLpCsAx78tzqY9jIr4MWyh9yHc&amp;embedded=true" width="720px"></iframe>

