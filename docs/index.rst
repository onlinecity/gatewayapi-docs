.. httppost documentation master file, created by
   sphinx-quickstart on Fri Aug  8 09:52:27 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GatewayAPI.com documentation
============================

Our documentation is broken into a few parts based on protocol.
All new customers are encouraged to use the new :ref:`rest`. However we also
provide a few legacy APIs based on SOAP and HTTP POST/GET, which are still
maintained although deprecated.

If you are reading this online at https://gatewayapi.com/docs/ and require any
assistence, please use the live chat feature at the bottom right of your screen.

.. toctree::
   :maxdepth: 3

   rest
   legacy
   soap
   errors
   glossary

You can also consult the `HTTP Routing Table <http-routingtable.html>`_

Writing and improving this documentation is an ongoing effort. Pull requests etc
are most welcome. https://github.com/onlinecity/gatewayapi-docs
